#!/usr/bin/env python

from mpipe import *


first  = lambda x: x[0]
second = lambda x: x[1]


def is_a_z(c):
	return "a" <= c <= "z"


blacklist = cat("blacklist.txt") | as_set

def is_not_blacklisted(s):
	return not (s.lower() | select(is_a_z) | concat("")) in blacklist


cat("words_frequency.txt") | \
	mappipe(str.split) | select(lambda x: len(x[0]) >= 5) | sort(key=lambda x: -int(x[1])) | \
	mappipe(first) | select(is_not_blacklisted) | dedup | take(10000) | stdout
