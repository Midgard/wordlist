#!/usr/bin/env python

import re
import sys
from pipe import *


# Input

stdin = (line.rstrip("\n") for line in sys.stdin)

def cat(*filenames):
	# This leaks the files, bad practice
	fhs = (
		open(filename, "r")
		for filename in filenames
	)
	return (
		line.rstrip("\n")
		for fh in fhs
		for line in fh
	)


# Output

# Overwrite pipe's stdout to leverage the power of generators
@Pipe
def stdout(pipein, *, file=sys.stdout, end="\n"):
	def my_print(x):
		print(x, file=file, end=end)

	if isinstance(pipein, str):
		my_print(pipein)

	try:
		for x in pipein:
			my_print(x)

	except TypeError:
		my_print(pipein)

	file.flush()


# Shell-like utils

@Pipe
def grep(pipein, pattern, flags=0):
	"""
	Like grep -E or egrep
	"""
	return (
		item
		for item in pipein
		if re.search(pattern, item, flags)
	)


@Pipe
def grepo(pipein, pattern, flags=0):
	"""
	Like grep -E -o
	"""
	for item in pipein:
		m = re.search(pattern, item, flags)
		if m:
			return m.group[0]


@Pipe
def fgrep(pipein, pattern):
	"""
	Like grep -F or fgrep
	"""
	return (
		item
		for item in pipein
		if pattern in item
	)


@Pipe
def column(pipein, selector, *, sep=None):
	def col_of_str(s):
		parts = s.split(sep)
		try:
			return tuple(parts[no] for no in selector)
		except TypeError:
			return parts[selector]

	if isinstance(pipein, str):
		return col_of_str(pipein)
	else:
		return (col_of_str(x) for x in pipein)


# Maps and filters

mappipe = select


@Pipe
def select(pipein, f):
	return filter(f, pipein)


# Misc

@Pipe
def inp(pipein, collection):
	try:
		return (
			item in collection
			for item in pipein
		)
	except TypeError:
		return pipein in collection
